import {VoteButtonType} from "./VoteButtonType";

export class DeputadosPartidosTable{
  img: string;
  name: string;
  vote: string;
  voteButton: VoteButtonType;
}
