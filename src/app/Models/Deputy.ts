export class Deputy{
    id : number;
    uri: string;
    nome: string;
    siglaPartido: string;
    uriPartido: string;
    siglaUf: string;
    urlFoto: string;
    email: string;
}