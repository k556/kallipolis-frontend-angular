import { PropostionCategory } from "./PropositionCategory";

export class Propostion{
  id: number;
  codTipo: string;
  numero:number;
  ano: number;
  ementa: string;
  temas: PropostionCategory[]
}
