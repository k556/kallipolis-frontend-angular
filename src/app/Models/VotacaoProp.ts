import {VotoPartidoProp} from "./VotoPartidoProp";
import {VotoDeputadoPropp} from "./VotoDeputadoProp";

export class VotacaoProp{
  id: string;
  uri: string;
  data: string;
  dataHoraRegistro: string;
  siglaOrgao: string;
  uriOrgao: string;
  idOrgao: number;
  uriEvento: string;
  idEvento: number;
  descricao: string;
  aprovacao: string;
  votosPartidos: VotoPartidoProp[];
  votosDeputados: VotoDeputadoPropp[];
}
