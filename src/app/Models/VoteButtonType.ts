export enum VoteButtonType {
  YES = 1,
  NO = 2,
  ABSTENCAO = 3
}
