import {VoteButtonType} from "./VoteButtonType";

export class VoteDeputy{
    votacao:string;
    voto: string;
    presencaNaSessao: string;
    justificativaAusencia: string;
    voteButton : VoteButtonType;
}
