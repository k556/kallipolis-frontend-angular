export class VotoPartidoProp {
  orientacaoVoto: string;
  codTipoLideranca: string;
  siglaPartidoBloco: string;
  codPartidoBloco: string;
  uriPartidoBloco: string;
}
