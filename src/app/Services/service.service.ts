import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Deputy } from '../Models/Deputy';
import { VoteDeputy } from '../Models/VoteDeputy';
import {VotacaoProp} from "../Models/VotacaoProp";
import { Propostion } from '../Models/Propostion';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  private headers: HttpHeaders;

  constructor(private http:HttpClient) {

  }
  // domain = "http://localhost:5000"; // http://kallipolisserver-env-1.eba-97xmevtw.us-east-1.elasticbeanstalk.com
  domain = "http://kallipolisserver-env.eba-czsf6jn4.us-east-1.elasticbeanstalk.com";

  voteDeputyUrl= this.domain + "/votacao/";
  deputyUrl =this.domain + "/deputado/62881";
  depUrl = this.domain + "/deputado";

  // http://kallipolisserver-env-1.eba-97xmevtw.us-east-1.elasticbeanstalk.com
  votePropUrl = this.domain + "/proposicao/";

  getDeputyInfo(idDep: string){
    // this.spinner
    return this.http.get<Deputy>(this.depUrl + "/"+idDep,
    {
      headers: this.headers
    });
    // this.sp
  }

  getDeputies(){
    // this.spinner
    return this.http.get<Deputy[]>(this.depUrl);
    // this.sp
  }

  getVotesPerDeputy(idDeputy: number){
    // this.spinner
    return this.http.get<VoteDeputy[]>(this.voteDeputyUrl + idDeputy + "/2021");
    // this.sp
  }

  getPropositionVotes(idDeputy: number){
    return this.http.get<VotacaoProp>(`${this.votePropUrl}${idDeputy}/votacao`);
  }

  getPropositionsPerDeputy(idDeputy: number){
    return this.http.get<Propostion[]>(`${this.votePropUrl}deputado/${idDeputy}`)
  }

  getVotesPerParty(){

  }

  getChartData(){

  }
}
