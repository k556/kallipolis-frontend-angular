import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PartyViewComponent } from './party-view/party-view.component';
import { PropositionViewComponent } from './proposition-view/proposition-view.component';
import {VoteDeputyComponent} from "./vote-deputy/vote-deputy.component";
import {VoteViewComponent} from "./vote-view/vote-view.component";


export const routes: Routes = [
  {path: 'party', component: PartyViewComponent },
  {path: 'votacao-prop-deputado/:id', component: VoteDeputyComponent },
  {path: 'votacao-prop/:id', component: VoteViewComponent },
  {path: 'proposicao-deputado/:id', component: PropositionViewComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
