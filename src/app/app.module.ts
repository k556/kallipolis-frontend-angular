import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CloudComponent } from './cloud/cloud.component';
import { TagCloudModule } from 'angular-tag-cloud-module';
import { PieChartComponent } from './pie-chart/pie-chart.component';
import { MatTabsModule } from '@angular/material/tabs';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatCardModule} from "@angular/material/card";
import { VoteViewComponent } from './vote-view/vote-view.component';
import { PartyViewComponent } from './party-view/party-view.component';
import {MatDividerModule} from "@angular/material/divider";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import { NgApexchartsModule } from "ng-apexcharts";
import { ServiceService } from './Services/service.service';
import { FormsModule } from '@angular/forms';
import {HttpClientModule} from '@angular/common/http'
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import { VoteDeputyComponent } from './vote-deputy/vote-deputy.component';
import { LoaderComponentComponent } from './loader-component/loader-component.component';
import { PropositionViewComponent } from './proposition-view/proposition-view.component';

@NgModule({
  declarations: [
    AppComponent,
    CloudComponent,
    PieChartComponent,
    VoteViewComponent,
    PartyViewComponent,
    VoteDeputyComponent,
    LoaderComponentComponent,
    PropositionViewComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        TagCloudModule,
        MatTabsModule,
        BrowserAnimationsModule,
        MatCardModule,
        MatDividerModule,
        MatButtonModule,
        MatIconModule,
        NgApexchartsModule,
        HttpClientModule,
        FormsModule,
        MatProgressSpinnerModule
    ],
  providers: [ServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
