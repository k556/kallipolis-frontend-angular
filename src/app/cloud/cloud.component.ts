import {Component, Input, OnInit} from '@angular/core';
import { CloudData, CloudOptions, ZoomOnHoverOptions  } from 'angular-tag-cloud-module';

@Component({
  selector: 'app-cloud',
  templateUrl: './cloud.component.html',
  styleUrls: ['./cloud.component.scss']
})

export class CloudComponent implements OnInit {

  options: CloudOptions = {
    // if width is between 0 and 1 it will be set to the width of the upper element multiplied by the value
    width: 360,
    // if height is between 0 and 1 it will be set to the height of the upper element multiplied by the value
    height: 400,
    overflow: false,
  };

  zoomOnHoverOptions: ZoomOnHoverOptions = {
    scale: 0.4, // Elements will become 130 % of current zize on hover
    transitionTime: 1.2, // it will take 1.2 seconds until the zoom level defined in scale property has been reached
    delay: 0.8 // Zoom will take affect after 0.8 seconds
  };

  @Input() cloudData:CloudData[]

  data: CloudData[] = [
    // { text: 'Weight-8-link-color', weight: 8},
    // { text: 'Weight-10-link', weight: 10, link: 'https://google.com', tooltip: 'display a tooltip'},
    // { text: 'weight-5-rotate(+10)', weight: 5, rotate: 10 },
    // { text: 'weight-7-rotate(-20)', weight: 7, rotate: -20 },
    // { text: 'weight-9-rotate(+35)', weight: 9, rotate: 35 }
    { text: 'black', weight: 1, rotate: Math.floor(Math.random() * 50) - 35},
    { text: 'white', weight: 2, rotate: Math.floor(Math.random() * 50) - 35 },
    { text: 'red', weight: 3, rotate: Math.floor(Math.random() * 50) - 35 },
    { text: 'green', weight: 4, rotate: Math.floor(Math.random() * 50) - 35 },
    { text: 'yellow', weight: 5, rotate: Math.floor(Math.random() * 50) - 35 },
    { text: 'blue', weight: 6, rotate: Math.floor(Math.random() * 50) - 35 },
    { text: 'pink', weight: 7, rotate: Math.floor(Math.random() * 50) - 35 },
    { text: 'gray', weight: 8, rotate: Math.floor(Math.random() * 50) - 35 },
    { text: 'brown', weight: 9, rotate: Math.floor(Math.random() * 50) - 35 },
    { text: 'orange', weight: 10, rotate: Math.floor(Math.random() * 50) - 35 },
    { text: 'purple', weight: 11, rotate: Math.floor(Math.random() * 50) - 35 }
  ];

  constructor() {
   }

  ngOnInit(): void {

  }
}
