import { Component, OnInit } from '@angular/core';

import { BehaviorSubject } from 'rxjs';
import {LoaderServiceService} from "../Services/loader-service.service";

@Component({
  selector: 'app-loader-component',
  templateUrl: './loader-component.component.html',
  styleUrls: ['./loader-component.component.scss']
})
export class LoaderComponentComponent implements OnInit {

  isLoading: BehaviorSubject<boolean> = this.loaderService.isLoading;

  constructor(private loaderService: LoaderServiceService) { }

  ngOnInit() {
  }

}
