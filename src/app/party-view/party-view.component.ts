import {Component, Input, OnChanges, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { VoteDeputy } from '../Models/VoteDeputy';
import { ServiceService } from '../Services/service.service';
import {DeputadosPartidosTable} from "../Models/DeputadosPartidosTable";
import {VoteButtonType} from "../Models/VoteButtonType";

@Component({
  selector: 'app-party-view',
  templateUrl: './party-view.component.html',
  styleUrls: ['./party-view.component.scss']
})
export class PartyViewComponent implements OnInit, OnChanges {

  @Input() votes: DeputadosPartidosTable[]
  voteButtonTypes = VoteButtonType;

  constructor (
    private service: ServiceService, 
    private router: Router
  ) {  }

  ngOnInit(): void {
  }

  ngOnChanges(){
    // this.currentVoteButton =
  }
}
