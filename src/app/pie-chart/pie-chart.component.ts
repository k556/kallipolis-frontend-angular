import {Component, Input, OnChanges, OnInit, ViewChild} from '@angular/core';

import { ChartComponent } from "ng-apexcharts";

import {
  ApexNonAxisChartSeries,
  ApexResponsive,
  ApexChart,
  ApexFill,
  ApexDataLabels,
  ApexLegend,
  ApexPlotOptions
} from "ng-apexcharts";

export type ChartOptions = {
  series: ApexNonAxisChartSeries;
  chart: ApexChart;
  responsive: ApexResponsive[];
  labels: any;
  fill: ApexFill;
  legend: ApexLegend;
  dataLabels: ApexDataLabels;
  plotOptions: ApexPlotOptions;
};

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.scss']
})

export class PieChartComponent implements OnInit, OnChanges {
  @ViewChild("chart") chart: ChartComponent;

  @Input() series:number[]
  @Input()  labels:string[]
  public chartOptions: Partial<ChartOptions> | any;

  constructor() {}

  ngOnInit(): void {
  }

  ngOnChanges(){
    this.chartOptions = {
      series: this.series,
      chart: {
        width: 330,
        type: "pie"
      },
      plotOptions: {
        pie: {
          dataLabels: {
            offset: -15,
          },
          donut: {
            labels: {
              show: false
            }
          }
        }
      },
      fill: {
        type: "gradient"
      },
      total: {
        showAlways: true,
        show: true
      },
      labels: this.labels,
      responsive: [
        {
          breakpoint: 480,
          options: {
            chart: {
              width: 330
            },
            legend: {
              position: "bottom"
            }
          }
        }
      ]
    };
  }
}
