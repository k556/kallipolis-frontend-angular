import { Component, OnInit } from '@angular/core';
import {VoteDeputy} from "../Models/VoteDeputy";
import {Propostion} from "../Models/Propostion";
import {VoteButtonType} from "../Models/VoteButtonType";
import {ServiceService} from "../Services/service.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Subscription} from "rxjs";
import {CloudData} from "angular-tag-cloud-module";
import {PropostionCategory} from "../Models/PropositionCategory";

@Component({
  selector: 'app-proposition-view',
  templateUrl: './proposition-view.component.html',
  styleUrls: ['./proposition-view.component.scss']
})
export class PropositionViewComponent implements OnInit {

  loading: boolean = true
  propositions: Propostion[]
  private routeSub: Subscription;
  private idDeputy: number;
  cloudData: CloudData[]

  constructor(private service: ServiceService, private route: ActivatedRoute, private router: Router) {
    this.propositions = []
    this.cloudData = []
  }

  ngOnInit(): void {
    this.loading = true

    this.routeSub = this.route.params.subscribe(params => {
      this.idDeputy = params['id'];
    });
    this.service.getPropositionsPerDeputy(this.idDeputy)
      .subscribe(data => {
          this.propositions = data.filter(d => d.codTipo != undefined);
          this.loading = false;
          this.computeCloud();
        },
        error => console.log('error = ', error)
      )
  }

  computeCloud(){
    let cloudColors = [
      '#000000',
      '#DCDCDC',
      '#000080',
      '#8B0000',
      '#20B2AA',
      '#9ACD32',
      '#8A2BE2',
      '#FF1493',
      '#FF4500',
      '#8B4513',
      '#FFA500',
      '#A020F0',
    ]

    let cloudValues: {[id: string] : number} = {}

    for (let i = 0; i < this.propositions.length; i++){
      for (let j = 0; j < this.propositions[i].temas.length; j++){
        let num = this.propositions[i].temas[j].tema;
        cloudValues[num] = cloudValues[num] ? cloudValues[num] + 1 : 1;
      }
    }

    let keys = cloudValues.keys;
    this.cloudData = []
    for (const key in cloudValues) {
      this.cloudData.push(
        { text: key, weight: cloudValues[key], color: cloudColors[Math.floor(Math.random() * 12)]}
      )
    }
  }
}
