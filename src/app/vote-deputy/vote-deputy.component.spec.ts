import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VoteDeputyComponent } from './vote-deputy.component';

describe('VoteDeputyComponent', () => {
  let component: VoteDeputyComponent;
  let fixture: ComponentFixture<VoteDeputyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VoteDeputyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VoteDeputyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
