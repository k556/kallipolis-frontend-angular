import {ChangeDetectorRef, Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {VoteDeputy} from '../Models/VoteDeputy';
import {ServiceService} from '../Services/service.service';
import {Subscription} from "rxjs";
import {VoteButtonType} from "../Models/VoteButtonType";

@Component({
  selector: 'app-vote-deputy',
  templateUrl: './vote-deputy.component.html',
  styleUrls: ['./vote-deputy.component.scss']
})
export class VoteDeputyComponent implements OnInit {

  votes: VoteDeputy[]
  loading: boolean = true
  private routeSub: Subscription;
  private idDeputy: number;
  series: number[]
  labels: string[]
  voteButtonTypes = VoteButtonType;

  countOfYes = 0;
  countOfNo = 0;
  countOfAbstencao = 0;

  constructor(private cdRef: ChangeDetectorRef, private service: ServiceService, private route: ActivatedRoute, private router: Router) {
    this.votes = []
    this.labels = []
    this.series = []
  }

  ngOnInit(): void {
    this.loading = true;
    this.routeSub = this.route.params.subscribe(params => {
      this.idDeputy = params['id'];
    });
    this.service.getVotesPerDeputy(this.idDeputy)
      .subscribe(data => {
          this.votes = data;
          this.votes.filter((element) => {
            if (element.voto.toLowerCase() === "sim") {
              this.countOfYes++;
              element.voteButton = VoteButtonType.YES
            } else if (element.voto.toLowerCase() === "não") {
              element.voteButton = VoteButtonType.NO
              this.countOfNo++;
            } else {
              element.voteButton = VoteButtonType.ABSTENCAO
              this.countOfAbstencao++;
            }
          })

          this.series = [ this.countOfYes, this.countOfNo, this.countOfAbstencao];
          this.labels = [
                         this.countOfYes + " votos sim", 
                         this.countOfNo + " votos não", 
                         this.countOfAbstencao + " abstenção"
                        ];
          this.cdRef.detectChanges();
          this.loading = false;
        },
        error => console.log('error = ', error)
      )
    }
}
