import {ChangeDetectorRef, Component, OnChanges, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ServiceService} from "../Services/service.service";
import {VotacaoProp} from "../Models/VotacaoProp";
import {DeputadosPartidosTable} from "../Models/DeputadosPartidosTable";
import {VoteButtonType} from "../Models/VoteButtonType";


@Component({
  selector: 'app-vote-view',
  templateUrl: './vote-view.component.html',
  styleUrls: ['./vote-view.component.scss']
})

export class VoteViewComponent implements OnInit, OnChanges {
  deputyButtonTriggered = false;
  partyButtonTriggered = false;

  votes: VotacaoProp
  loading: boolean = true
  private idProp: number;
  series: number[]
  labels: string[]
  votesTable: DeputadosPartidosTable[]
  isEmpty: boolean

  constructor(private cdRef: ChangeDetectorRef, private service:ServiceService, private route: ActivatedRoute, private router : Router) {
    this.labels = []
    this.series = []
    this.votesTable = []
    this.isEmpty = false;
   }

    Deputados(){
      this.deputyButtonTriggered = true;
      this.partyButtonTriggered = false;
      // this.router.navigate(["deputy"]);
      this.deputadosData();
    }

    Partidos(){
      this.deputyButtonTriggered = false;
      this.partyButtonTriggered = true;
      this.partidosData();
      // this.router.navigate(["party"]);
    }

  ngOnInit(): void {
    this.deputyButtonTriggered = true;
    this.loading = true;
    this.route.params.subscribe(params => {
      this.idProp = params['id'];
    });
    if(this.votes == null){
      this.initalFecth();
    }
  }

  ngOnChanges() {
    this.Partidos();
  }

  initalFecth(): void {
    this.loading = true;
      this.service.getPropositionVotes(this.idProp)
        .subscribe(data => {
          this.votes = data;
          this.deputadosData();
          this.loading = false;
        }, error => console.log('test = ', error));
  }

  fetchVotes(): void {
    this.loading = true;
    this.service.getPropositionVotes(this.idProp)
      .subscribe(data => {
        this.votes = data;
        this.loading = false;
      }, error => console.log('test = ', error));
  }

  partidosData(){
    if(this.votes == null){
      this.fetchVotes();
    }

    if(this.votes.votosPartidos == null || this.votes.votosPartidos.length == 0){
      this.isEmpty = true;
      return;
    }

    let countOfYes = 0;
    let countOfNo = 0;
    let countOfAbstencao = 0;

    this.votesTable = [];
    this.votes.votosPartidos.forEach(element => {
      this.votesTable.push(
        <DeputadosPartidosTable>{
          img: "https://upload.wikimedia.org/wikipedia/commons/b/bf/Coat_of_arms_of_Brazil.svg",
          name: element.siglaPartidoBloco,
          vote: element.orientacaoVoto.toUpperCase(),
          voteButton: element.orientacaoVoto.toLowerCase() === "sim" ? VoteButtonType.YES : VoteButtonType.NO
        }
      );

      if (element.orientacaoVoto.toLowerCase() === "sim") {
        countOfYes++;
      } else if (element.orientacaoVoto.toLowerCase() === "não") {
        countOfNo++;
      } else {
        countOfAbstencao++;
      }
    })

    this.series = [ countOfYes, countOfNo, countOfAbstencao];
    this.labels = [
                   countOfYes + " orientação sim", 
                   countOfNo + " orientação não", 
                   countOfAbstencao + " abstenção"
                  ];

    this.cdRef.detectChanges();
  }

  deputadosData(){
    if(this.votes.votosDeputados == null || this.votes.votosDeputados.length == 0){
      this.isEmpty = true;
      return;
    }
    let countOfYes = 0;
    let countOfNo = 0;
    let countOfAbstencao = 0;

    this.votesTable = []
    this.votes.votosDeputados.forEach(element => {
      this.votesTable.push(
        <DeputadosPartidosTable>{
          img: element.deputado_.urlFoto,
          name: element.deputado_.nome,
          vote: element.tipoVoto.toUpperCase(),
          voteButton: element.tipoVoto.toLowerCase() === "sim" ? VoteButtonType.YES : VoteButtonType.NO
        }
      );

      if (element.tipoVoto.toLowerCase() === "sim") {
        countOfYes++;
      } else if (element.tipoVoto.toLowerCase() === "não") {
        countOfNo++;
      } else {
        countOfAbstencao++;
      }
    })

    this.series = [ countOfYes, countOfNo, countOfAbstencao];
    this.labels = [
                   countOfYes + " votos sim", 
                   countOfNo + " votos não", 
                   countOfAbstencao + " abstenção"
                  ];

    this.cdRef.detectChanges();
  }
}
